package com.gaido.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.junit.Test;


public class ConvertPngToJpgTest {

	@Test
	public void testToJpg() throws Throwable {
		try {
			final File file = new File(getClass().getClassLoader().getResource("howMyDogLearndPolymorphism.PNG").getFile());
			final BufferedImage im = ImageIO.read(file);

			final String outputFilename = "src/test/resources/howMyDogLearndPolymorphism.PNG_to.jpg";
			final OutputStream output = new FileOutputStream(outputFilename);
			ConvertPngToJpg.toJpg(im, output);
			output.close();
			Assert.assertNotNull(output);
			Assert.assertTrue(new File(outputFilename).length() > 0);


		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToJpg2() throws Throwable {
		try {
			testToJpg("col_d_Aubisque.bmp");
			testToJpg("col_d_Aubisque.gif");
			testToJpg("col_d_Aubisque.jpg");
			testToJpg("col_d_Aubisque.png");
			testToJpg("col_d_Aubisque.tif");

		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	void testToJpg(String inputName) throws IOException  {
		final File file = new File(getClass().getClassLoader().getResource(inputName).getFile());
		final BufferedImage im = ImageIO.read(file);

		final String outputFilename = "src/test/resources/" + inputName + "_to.jpg";
		final OutputStream output = new FileOutputStream(outputFilename);
		ConvertPngToJpg.toJpg(im, output);
		output.close();
		Assert.assertNotNull(output);
		Assert.assertTrue(new File(outputFilename).length() > 0);


	}





}
