package com.gaido.test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

public class ConvertPngToJpg {

	private ConvertPngToJpg() {}

	/**
	 * Convert an IMAGE (of any format supported by ImageIO implementation) to a JPG image.
	 * @param bufferedImage Input image.
	 * @param outputStream Output JPEG image.
	 * @throws IOException If an error occurs during writing or when not able to create required ImageOutputStream. 
	 */
	public static void toJpg(final BufferedImage bufferedImage, final OutputStream outputStream) throws IOException {

		if (bufferedImage==null) {
			throw new IllegalArgumentException("bufferedImage == null!");
		}
		if (outputStream==null) {
			throw new IllegalArgumentException("outputStream == null!");
		}

		final BufferedImage opaqueImage = ensureOpaque(bufferedImage);
		final String formatName = "JPEG";
		ImageIO.write(opaqueImage, formatName, outputStream);
	}

	/**
	 * https://stackoverflow.com/questions/3432388/imageio-not-able-to-write-a-jpeg-file
	 * 
	 * author : Rui Vieira
	 * 2012 answer
	 * OpenJDK does not have a native JPEG encoder, try using Sun's JDK, or using a library (such as JAI) 
	 * AFAIK, regarding the "pinkish tint", Java saves the JPEG as ARGB (still with transparency information). 
	 * Most viewers, when opening, assume the four channels must correspond to a CMYK (not ARGB)
	 * and thus the red tint.
	 * 
	 * 
	 * author : Adam Gawne-Cain 
	 * 2019 answer 
	 * Make sure your BufferedImage does not have alpha transparency. 
	 * JPEG does not support alpha, so if your image has alpha then ImageIO cannot write it to JPEG.
	 * Use the following code to ensure your image does not have alpha transparancy :
	 * 
	 */
	static BufferedImage ensureOpaque(BufferedImage bi) {
		if (bi.getTransparency() == BufferedImage.OPAQUE)
			return bi;
		int w = bi.getWidth();
		int h = bi.getHeight();
		int[] pixels = new int[w * h];
		bi.getRGB(0, 0, w, h, pixels, 0, w);
		BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		bi2.setRGB(0, 0, w, h, pixels, 0, w);
		return bi2;
	}

}
